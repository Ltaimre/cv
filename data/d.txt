{ "eesti": [
{
   "header": "Mina",
   "class": "smallbox",
   "list": ["Liisi Taimre", "1985", "nõunik-turundusjuht"]
},

{
  "header": "Haridus",
  "class": "bigbox",
  "list": ["IT kolledz. Arendaja. II kursus (2015 – ?)", "Tartu Ülikool. Ajalugu. MA. (2010)", "Gustav Adolfi Gümnaasium (2004)"]
},


{
  "header": "Keeled",
  "class": "smallbox",
  "list": ["HTML, CSS, JS", "Java", "(C#)"]
},


{
  "header": "Tugevused",
  "class": "bigbox",
  "list": ["loomingulisus", "kiire õppimisvõime", "järjekindlus"]
},

{
  "header": "Nõrkused",
  "class": "smallbox",
  "list": ["vähene kogemus", "peast arvutamine :)"]

},

{
  "header": "Hobid",
  "class": "smallbox",
  "list": ["orienteerumine", "lugemine", "viinamarjakasvatus" ]

},

{
  "header": "Kontakt",
  "class": "smallbox",
  "list": ["liisitaimre@gmail.com", "5253892", "skype: ltaimre"]
},


{
  "header": "Motivatsioon",
  "class": "bigbox",
  "list": ["meeldib õppida uusi asju", "loodan saada praktilist kogemust",  "tahan teada saada, kuidas valmivad IT-arendused" ]
},


{
  "header": "Lähtekood",
  "class": "smallbox",
  "link": "https://bitbucket.org/Ltaimre/cv/src",
  "linkname": "CV lähtekood"
}



],

"english": [
{
   "header": "Me",
   "class": "smallbox",
   "list": ["Liisi Taimre", "1985", "marketing manager"]
},

{
  "header": "Education",
  "class": "bigbox",
"list": ["IT College. Developer. Second year (2015 – ?)", "Tartu University. History. MA.", "Gustav Adolf Grammar School (2004)"]
},


{
  "header": "Languages",
  "class": "smallbox",
  "list": ["HTML, CSS, JS", "Java",  "(C#)"]
},


{
  "header": "Strengths",
  "class": "bigbox",
  "list": ["creativity", "fast learner", "consistency"]
},

{
  "header": "Weaknesses",
  "class": "smallbox",
  "list": ["lack of experience"]

},

{
  "header": "Hobbies",
  "class": "smallbox",
  "list": ["orienteering", "reading", "growing grapes" ]

},

{
  "header": "Contact",
  "class": "smallbox",
  "list": ["liisitaimre@gmail.com", "5253892", "skype: ltaimre"]
},


{
  "header": "Motivation",
  "class": "bigbox",
  "list": ["I like to learn new things", "I would like to gain some practical experience",  "I want to know how IT developments are made" ]
},


{
  "header": "Source code",
  "class": "smallbox",
  "link": "https://bitbucket.org/Ltaimre/cv/src",
  "linkname": "CV source coude"
}



]


}