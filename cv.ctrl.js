(function () {
    'use strict';

    var app = angular.module('myApp').controller('CVCtrl', Ctrl);

    function Ctrl ($http) {

        var vm=this;
        this.labels=[];
        this.language="";
        this.toEst=toEst;


    init();

    function init() {
        $http.get("data/d.txt")
            .then(function(response) {
                vm.labels = response.data.english;


            });
        vm.language="Eesti keeles";
    }

        function toEst() {
        if (vm.language==="Eesti keeles") {
            $http.get("data/d.txt")
                .then(function(response) {
                    vm.labels = response.data.eesti;


                });
            vm.language="In English";

        }
        else {
            init();
                    }

        }

    };





})();
